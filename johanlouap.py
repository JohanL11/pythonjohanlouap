#Exercice 1
def secInSiecle():
  ans=100
  jourParAn=365
  heureParJour=24
  minParHeure=60
  secParMin=60

  secParSiecle=ans*jourParAn*heureParJour*minParHeure*secParMin

  return secParSiecle
 
#Exercice 2
def paraRectangle():
  longueur=10
  largeur=8
  profondeur=3

  volume=longueur*largeur*profondeur

  return volume

#Exercice 3
def aire():
  pi=3.14
  R=10
  Rcarre = R**2

  aireDisque=pi*Rcarre

  return aireDisque
  
#Exercice 4
prenom = input('Entrez votre prénom : ')
age = input('Entrez votre age : ')
floatAge = float(age)

phraseAge = 'Tu es mineur !'
if floatAge>=18:
  phraseAge = 'Tu es majeur !'

print ('Bonjour', prenom,'.', phraseAge,)

#Exercice 5
def capital():
  cap=1000
  interet=1.10

  for i in range(1,4):
    cap = cap*interet;
  return cap
  
#Exercice 6
def change():
  a=9
  b=11
  c=a
  a=b
  b=c

  toto = 'a',a,'b',b,
  return toto
  
#Exercice 7
from math import sqrt

def a():
  for i in range(1,100):
    print (i**2)

def b():
  for i in range(10,20):
    print (i**4)

def c():
  for i in range(1,100):
    if i%5==0:
      print (sqrt(i))
      
#Exercice 8
def multi():
  for i in range(1,10):
    for j in range(1,10):
      print (i,'x',j,'=',i*j,)
      
#Exercice 9
import random

def exo9():
  a = random.randint(1,12)
  b = random.randint(1,12)
  
  resultUser = input('Combien vaut '+str(a)+' x '+str(b)+' ? ')

  if int(resultUser) == a*b:
    print('Bravo !')
  else:
    print ('Perdu ! La réponse était',a*b,)
    
#Exercice 10
def exo10():
  for i in range(1,999):
    string = str(i)
    if '3' == string[-1]:
      somme = 0
      if len(string) == 1:
        somme = string
        dizaine = 0
      elif len(string) == 2:
        somme = string[0] + string[1]
        dizaine = string[1]
      else:
        somme = int(string[0]) + int(string[1]) + int(string[2])
        dizaine = string[1]
      if int(somme) >= 15:
        if int(dizaine)%2 == 0:
          print (i)
  
#Exercice 11
from random import randint

def jeu(nbUser, nbAleatoire):
  if nbAleatoire < nbUser:
    print ("Le nombre à trouver est plus petit !\n")
  elif nbAleatoire > nbUser:
    print ("Le nombre à trouver est plus grand !\n")
  else :
    print ("Bravo, c'est le bon nombre !")
    
nbUser = 0
nbAleatoire = randint(1, 99)
while nbUser != nbAleatoire:
  nbUser = int(input("Entrez un nombre entre 1 et 99 : "))
  jeu(nbUser,nbAleatoire)

#Exercice 12
#1
def affiche_table_de_7():
  for i in range(1,11):
    print(i,'x 7 =',i*7)

def affiche_bonjour():
  prenom = input('Entrez votre prénom : ')
  print('Bonjour',prenom,'!')
 
#2
def affiche_une_table(n):
  for i in range(1,11):
    print(i,'x',n,'=',i*n)

def affiche_salutation(formule):
  prenom = input('Entrez votre prénom : ')
  print(formule,prenom,'!')
  
#Exercice 13
def trinome_1(x):
  xCarre = x**2
  return (3 * xCarre) - (7 * x) + 4

def trinome_2(a,b,c,x):
  xCarre = x**2
  return (a * xCarre) - (b * x) + c

def conversion_euros_vers_dollars(montant):
  return montant*1.15

def conversion_euros(montant,devise):
  if devise == 'dollar':
    return montant*1.15
  elif devise == 'livre':
    return montant*0.81
  elif devise == 'yens':
    return montant*130
  else:
    return "Devise inconnue"

#Exercice 16
def distance_hamming(mot1, mot2):
  compteur=0
  for i in range(len(mot1)):
    if(mot1[i] != mot2[i]):
      compteur+=1
  return compteur

#Exercice 17
def inserver(mot):
  return mot[::-1] 
  
def isPalindrome(mot): 
  motInverse = inserver(mot) 

  if (mot == motInverse): 
    return True
  return False

#Exercice 18
ADNmoutarde = "CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGCAGCADN"

ADNrose = "CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGGADN"

ADNpervenche = "AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCCADN"

ADNleblanc = "CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG"

allADN = [ADNmoutarde, ADNrose, ADNpervenche, ADNleblanc]

for i in allADN:
  if 'CATA' in i:
    newChaine = i.replace('CATA','')
    if 'ATGC' in newChaine:
      if i == "CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGCAGCADN":
        nom = 'Moutarde'
      elif i == "CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGGADN":
        nom = 'Rose'
      elif i == "CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGGADN":
        nom = 'Pervenche'
      else:
        nom = "Le Blanc"
      print(nom,'est coupable !')